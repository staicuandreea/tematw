import React from 'react';
import './ProductList.css'

export class ProductList extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            source: []
        };
    }
    
    render(){
        let products=this.props.source.map((product, index)=>
            <div key={index}>{product.id}</div>
        );
        return (
            <React.Fragment>
            <div>
                <h1>{this.props.title}</h1>
                <div className="products-container">{products}</div>
            </div>
            </React.Fragment>
            )
    }
}