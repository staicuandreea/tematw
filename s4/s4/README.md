# Subiectul 4: React

# Se va folosi API-ul `product-api` ce are implementate metodele `POST` pentru a adauga produse si `GET` pentru a obtine lista completa de produse.
# Sa se modifice proiectul `product-client` astfel incat se vor adauga urmatoarele componente:
- `AddProduct` pentru a adauga produse noi;
- `ProductList` pentru a afisa lista de produse;