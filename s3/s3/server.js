const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

//update pe baza id-ului
app.put('/products/:id/update',(req,res)=>{
   const id=req.params.id;
   let updated=false;
   products.forEach((product)=>{
       if(product.id==id){
           updated=true;
           product.price=req.body.price;
           product.productName=req.body.productName;
       }
   });
   if(updated){
       res.status(200).send(`Todo ${id} updated!`);
   }else{
       res.status(404).send(`Could not find resource with id ${id}!`);
   }
});

//delete pe baza numelui
app.delete('/delete-product',(req,res)=>{
   const prodName=req.params.productName;
   const prod=products.filter(product=>{
       return prod.productName==prodName;
   })[0];
   
   const index = products.indexOf(prod);
   
   if(index<products.length){
       products.splice(index,1);
       res.status(200).send(`Product with id ${index+1} was deleted.`);
   }else{
       res.status(404).send(`Could not find any item with id ${index+1}.`);
   }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});